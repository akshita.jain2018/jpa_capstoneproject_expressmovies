package com.mov.repository;

import java.sql.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.mov.entity.Movie;

	public interface MovieRepository extends JpaRepository<Movie, Long>{
		
		@Transactional
		   @Modifying(clearAutomatically=true, flushAutomatically=true)
		   @Query(value = "UPDATE Movie SET date_released += :dateReleased WHERE movie_title += :movieTitle",nativeQuery = true)
		   public void dateReleasedUpdate(Date dateReleased,String movieTitle );
		@Transactional
		   @Modifying(clearAutomatically=true, flushAutomatically=true)
		   @Query(value = "SELECT * FROM Movie WHERE movieTitle += :movieTitle ",nativeQuery = true)
		   List<Movie> findByMovieTitle(String movieTitle);
		   public void removeByMovieTitle(String movieTitle);
		
}
