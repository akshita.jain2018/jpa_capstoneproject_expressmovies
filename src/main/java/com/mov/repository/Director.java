package com.mov.repository;

import java.sql.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.mov.entity.Director;

	@SuppressWarnings("unused")
	public interface DirectorRepository  extends JpaRepository<Director, Long>{
		@Transactional
		   @Modifying(clearAutomatically=true, flushAutomatically=true)
		   @Query(value = "UPDATE director SET address += :address , contact_number += :contactNumber WHERE first_name += :firstName and last_name += :lastName",nativeQuery = true)
		   public void adressAndContactUpdate(String firstName,String lastName,String address,Long contactNumber );
		   
	}
