package com.mov.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mov.dto.MovieDTO;
import com.mov.entity.Movie;
import com.mov.repository.MovieRepository;


	@Service("movieService")
	public class MovieServiceImpl implements MovieService {
		
		static Logger logger = Logger.getLogger(MovieServiceImpl.class);
		        
				@Autowired
				private MovieRepository repository;
				@Override
				public void insertMovie(MovieDTO Movie) {
					repository.saveAndFlush(MovieDTO.prepareMovieEntity(Movie));
					logger.info("Records are successfully added in Movie table..");
				}
				@Override
				public List<Movie> viewMovie() {
					   
				       List<Movie> movie= repository.findAll();
				       return movie;
				}

				@Override
				public void removeMovieByTitle(String movieTitle)throws Exception{
					try{
						repository.removeByMovieTitle(movieTitle);
						logger.info("Record is successfully deleted from Movie table...");
					}
					catch (Exception e) {
						logger.info("In log Exception and Movie with the given Title  does not exist ");
						logger.error(e.getMessage(),e);
					}
				}

				@Override
				public MovieDTO getMovie( Long movieId) {
					Optional<Movie> optionalMovie= repository.findById(movieId);
					Movie movieEntity = optionalMovie.get();// Converting Optional<Customer> to Customer
					MovieDTO movieDTO = Movie.prepareMovieDTO(movieEntity);
					return movieDTO;
				}
				@Override
				public String updateMovie(Long movieId,LocalDate dateReleased) {
					Optional<Movie> optionalMovie = repository.findById(movieId);
					Movie movieEntity = optionalMovie.get();
					movieEntity.setDateReleased(dateReleased);  
					repository.save(movieEntity);
					return "The Movie with movie id :" + movieId + " has been updated successfully.";
				}
				@Override
				public void dateReleasedUpdate(Date dateReleased,String movieTitle ) {
					repository.dateReleasedUpdate(dateReleased, movieTitle);
				}
			    public Iterable<Movie> getMovieByTitle(String movieTitle) {
			    	Iterable<Movie> movie=null;
			    	try{
			    		movie= repository.findByMovieTitle(movieTitle);
					}
					catch (Exception e) {
						logger.info("In log Exception and Not able to get the Movie as the Movie  with the given Title  does not exist ");
						logger.error(e.getMessage(),e);
					}
			    	return movie;
				}
}
