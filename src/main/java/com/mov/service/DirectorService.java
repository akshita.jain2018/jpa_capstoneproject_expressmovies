package com.mov.service;

import java.util.List;
import com.mov.dto.DirectorDTO;
import com.mov.entity.Director;

	public interface DirectorService {

		public void insertDirector(DirectorDTO Director);
		public List<Director> viewDirector() ;
		public DirectorDTO getDirector(Long directorId);
		public String updateDirector(Long directorId,String address,Long contactNumber,String email,String firstName,String lastName);
		public void adressAndContactUpdate(String firstName,String lastName,String address,Long contactNumber );
}
