package com.mov.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import com.mov.dto.MovieDTO;
import com.mov.entity.Movie;

	public interface MovieService {
			
				public void insertMovie(MovieDTO Movie) ;
				public List<Movie> viewMovie();
				public MovieDTO getMovie(Long movieId);
				public String updateMovie(Long movieId,LocalDate dateReleaased);
				public void dateReleasedUpdate(Date dateReleased,String movieTitle );
				public Iterable<Movie> getMovieByTitle(String movieTitle);
				public void removeMovieByTitle(String movieTitle)throws Exception;
}
