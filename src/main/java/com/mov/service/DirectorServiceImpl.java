package com.mov.service;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mov.dto.DirectorDTO;
import com.mov.entity.Director;
import com.mov.repository.DirectorRepository;

	@Service("directorService")
	public class DirectorServiceImpl  implements DirectorService{
		            static Logger logger = Logger.getLogger(DirectorServiceImpl.class);
		
					@Autowired
					private DirectorRepository repository;
			
					@Override
					public void insertDirector(DirectorDTO Director) {
						repository.saveAndFlush(DirectorDTO.prepareDirectorEntity(Director));
						logger.info("Records are successfully added in Directors table..");
					}
				
					public DirectorDTO getDirector(Long directorId) {
						Director directorEntity=repository.findById(directorId).get();
						DirectorDTO directorDTO =Director.prepareDirectorDTO(directorEntity);
						return  directorDTO;
					}
					 public void adressAndContactUpdate(String firstName,String lastName,String address,Long contactNumber ) {
						 repository.adressAndContactUpdate(firstName, lastName, address, contactNumber);
					 }
					public List<Director> viewDirector() {
					       List<Director> director= repository.findAll();
					       return director;
					}
					
					public String updateDirector(Long directorId,String address,Long contactNumber,String email,String firstName,String lastName) {
						Director directorEntity=repository.findById(directorId).get();
						directorEntity.setAddress(address);
						directorEntity.setContactNumber(contactNumber);
						directorEntity.setEmail(email);
						return "The details of the director with firstname : "+ firstName + "and lastname : " + lastName + " has been updated successfully.";
					}
}
