package com.mov.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import com.mov.dto.MovieDTO;

	@SuppressWarnings("unused")
	@Entity
	public class Movie {

	    @Id
	    @Column(name ="movie_id")
		private Long movieId;
	    @Column(name ="movie_title")
		private String movieTitle;
	    @Column(name ="date_released")
		private LocalDate DateReleased;
	    @Column(name ="movie_running_time")
	    private LocalTime movieRunningTime;
		
	    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	    @JoinTable(
	    		name = "movie_director",
	    		joinColumns = @JoinColumn(name = "movie_id",referencedColumnName = "movie_id"),
	    		inverseJoinColumns = @JoinColumn(name = "director_id",referencedColumnName="director_id")
	    		)
	    private List<Director> directors;
		
		public List<Director> getDirectors() {
			return directors;
		}
		
		public void setDirectors(List<Director> directors) {
			this.directors = directors;
		}
		
		public Movie() {
			super();
		}
		
		public Long getMovieId() {
			return movieId;
		}
		
		public Movie(Long movieId, String movieTitle, LocalDate dateReleased, LocalTime movieRunningTime,
				List<Director> directors) {
			super();
			this.movieId = movieId;
			this.movieTitle = movieTitle;
			DateReleased = dateReleased;
			this.movieRunningTime = movieRunningTime;
			this.directors = directors;
		}
		
		public void setMovieId(Long movieId) {
			this.movieId = movieId;
		}
		
		public String getMovieTitle() {
			return movieTitle;
		}
		
		public void setMovieTitle(String movieTitle) {
			this.movieTitle = movieTitle;
		}
		
		public LocalDate getDateReleased() {
			return DateReleased;
		}
		
		public void setDateReleased(LocalDate dateReleased) {
			DateReleased = dateReleased;
		}
		
		public LocalTime getMovieRunningTime() {
			return movieRunningTime;
		}
		
		public void setMovieRunningTime(LocalTime movieRunningTime) {
			this.movieRunningTime = movieRunningTime;
		}
		
		@Override
		public String toString() {
			return "Movie [movieId=" + movieId + ", movieTitle=" + movieTitle + ", DateReleased=" + DateReleased
					+ ", movieRunningTime=" + movieRunningTime + "]";
		}

		public static MovieDTO prepareMovieDTO(Movie movie)
		{
			MovieDTO movieDTO = new MovieDTO();
			movieDTO.setMovieId(movie.getMovieId());
			movieDTO.setMovieTitle(movie.getMovieTitle());
			movieDTO.setDateReleased(movie.getDateReleased());
			movieDTO.setMovieRunningTime(movie.getMovieRunningTime());
			return movieDTO;
		}
}
