package com.mov.entity;

import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import com.mov.dto.DirectorDTO;

	@Entity
	public class Director {

		@Id
	    @Column(name ="director_id")
		private Long directorId;
		@Column(name ="first_name")
		private String firstName;
		@Column(name ="last_name")
		private String lastName;
		private String address;
		@Column(name ="contact_number")
		private Long contactNumber;
		private String email;
		
		@ManyToMany(mappedBy = "directors" ,cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
		private List<Movie> movies ;
		
		public Director() {
			super();
		}
		
		public Director(Long directorId, String firstName, String lastName, String address, Long contactNumber,
				String email, List<Movie> movies) {
			super();
			this.directorId = directorId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.address = address;
			this.contactNumber = contactNumber;
			this.email = email;
			this.movies = movies;
		}

		public Long getDirectorId() {
			return directorId;
		}
		
		public void setDirectorId(Long directorId) {
			this.directorId = directorId;
		}
		
		public String getFirstName() {
			return firstName;
		}
		
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		
		public String getLastName() {
			return lastName;
		}
		
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public Long getContactNumber() {
			return contactNumber;
		}
		public void setContactNumber(Long contactNumber) {
			this.contactNumber = contactNumber;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		@Override
		public String toString() {
			return "Director [directorId=" + directorId + ", firstName=" + firstName + ", lastName=" + lastName
					+ ", address=" + address + ", contactNumber=" + contactNumber + ", email=" + email + "]";
		}
//		public static CustomerDTO prepareCustomerDTO(Customer customer)
		public static DirectorDTO prepareDirectorDTO(Director director)
		{
			DirectorDTO directorDTO = new DirectorDTO();
			directorDTO.setDirectorId(director.getDirectorId());
			directorDTO.setFirstName(director.getFirstName());
			directorDTO.setLastName(director.getLastName());
			directorDTO.setAddress(director.getAddress());
			directorDTO.setContactNumber(director.getContactNumber());
			directorDTO.setEmail(director.getEmail());
			return directorDTO;
		}
		public List<Movie> getMovies() {
			return movies;
		}
		public void setMovies(List<Movie> movies) {
			this.movies = movies;
		}
}
