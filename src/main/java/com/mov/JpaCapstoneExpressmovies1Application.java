package com.mov;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import com.mov.dto.DirectorDTO;
import com.mov.dto.MovieDTO;
import com.mov.entity.Director;
import com.mov.entity.Movie;
import com.mov.repository.DirectorRepository;
import com.mov.service.DirectorService;
import com.mov.service.MovieService;

@SuppressWarnings("unused")
@SpringBootApplication
public class JpaCapstoneExpressmovies1Application implements CommandLineRunner{

		static Logger logger = Logger.getLogger(JpaCapstoneExpressmovies1Application.class);
		@Autowired
		ApplicationContext context;
		@Autowired
		DirectorService service;
		@Autowired
		MovieService service1;
//		@SuppressWarnings("unused")
		@Autowired
		private DirectorRepository repository;
		
		public static void main(String[] args) {
			SpringApplication.run(JpaCapstoneExpressmovies1Application.class, args);
		}

		@Override
		public void run(String... args) throws Exception {
			LocalDate date1 = LocalDate.parse("2018-05-05");
			LocalDate date2 = LocalDate.parse("2019-06-15");
			LocalTime time1 =LocalTime.parse("19:20:23");
			LocalTime time2 = LocalTime.parse("18:22:20");
			LocalTime time3 = LocalTime.parse("20:21:10");
			LocalDate date3 = LocalDate.parse("2022-06-02");
			MovieDTO movie1 = new MovieDTO(1L,"Lock and key",date1,time1);
			MovieDTO movie2 = new MovieDTO(2L,"Harry Potter",date2,time1);
			MovieDTO movie3 = new MovieDTO(3L,"Jai Hind",date3,time3);
			DirectorDTO dir1 = new DirectorDTO(1L,"Akshita","Jain","Agra,UttarPradesh",7022713754L,"abc@gmail.com");
			DirectorDTO dir2 = new DirectorDTO(2L,"Richard","Chatterjee","Indore,MAdhya Pradesh",7022713744L,"efg@gmail.com");
			DirectorDTO dir3 = new DirectorDTO(3L,"Daniel","Verma ","Mumbai,Maharashtra",7022713354L,"xyz@gmail.com");

			service.insertDirector(dir1);
			service.insertDirector(dir2);
	        service.insertDirector(dir3);

	        service1.insertMovie(movie1);
			service1.insertMovie(movie2);
			service1.insertMovie(movie3);
			
	        service.viewDirector();
	        service1.viewMovie();
	        
	        service1.getMovie(2L);
	        
	        service.updateDirector(1L, "Indore,Madhya Pradesh", 7022713744L,"efg@gmail.com","Richard","Chatterjee");
	        service.adressAndContactUpdate("Akshita", "Jain", "Agra,UttarPradesh", 7022713754L);
	        service1.updateMovie(1L, date2);
	        
	        service1.removeMovieByTitle("Harry Potter");
		}
}