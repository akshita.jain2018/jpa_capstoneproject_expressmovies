package com.mov.dto;

import com.mov.entity.Director;

	public class DirectorDTO {
		private Long directorId;
		private String firstName;
		private String lastName;
		private String address;
		private Long contactNumber;
		private String email;
		
		public DirectorDTO() {	
		}
		
		public DirectorDTO(Long directorId, String firstName, String lastName, String address, Long contactNumber,
				String email) {
			super();
			this.directorId = directorId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.address = address;
			this.contactNumber = contactNumber;
			this.email = email;
		}
		
		public Long getDirectorId() {
			return directorId;
		}
		
		public void setDirectorId(Long directorId) {
			this.directorId = directorId;
		}
		
		public String getFirstName() {
			return firstName;
		}
		
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		
		public String getLastName() {
			return lastName;
		}
		
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		
		public String getAddress() {
			return address;
		}
		
		public void setAddress(String address) {
			this.address = address;
		}
		
		public Long getContactNumber() {
			return contactNumber;
		}
		
		public void setContactNumber(Long contactNumber) {
			this.contactNumber = contactNumber;
		}
		
		public String getEmail() {
			return email;
		}
		
		public void setEmail(String email) {
			this.email = email;
		}
		
		@Override
		public String toString() {
			return "DirectorDTO [directorId=" + directorId + ", firstName=" + firstName + ", lastName=" + lastName
					+ ", address=" + address + ", contactNumber=" + contactNumber + ", email=" + email + "]";
		}
		
		public static Director prepareDirectorEntity(DirectorDTO directorDTO)
		{
			Director directorEntity = new Director();
			directorEntity.setDirectorId(directorDTO.getDirectorId());   
			directorEntity.setFirstName(directorDTO.getFirstName()); 
			directorEntity.setLastName(directorDTO.getLastName());  
			directorEntity.setAddress(directorDTO.getAddress());    
			directorEntity.setContactNumber(directorDTO.getContactNumber());
			directorEntity.setEmail(directorDTO.getEmail());
			return directorEntity;
		}
}
