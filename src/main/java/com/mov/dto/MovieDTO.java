package com.mov.dto;

import java.util.Date;
import com.mov.entity.Movie;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;

	@SuppressWarnings("unused")
	public class MovieDTO {

		private Long movieId;
		private String movieTitle;
		private LocalDate dateReleased;
		private LocalTime movieRunningTime;
		
		public MovieDTO() {	
		}
		
		public MovieDTO(Long movieId, String movieTitle, LocalDate i,LocalTime movieRunningTime) {
			super();
			this.movieId = movieId;
			this.movieTitle = movieTitle;
			this.dateReleased = i;
			this.movieRunningTime = movieRunningTime;
		}
		
		public Long getMovieId() {
			return movieId;
		}
		
		public void setMovieId(Long movieId) {
			this.movieId = movieId;
		}
		
		public String getMovieTitle() {
			return movieTitle;
		}
		
		public void setMovieTitle(String movieTitle) {
			this.movieTitle = movieTitle;
		}
		
		public LocalDate getDateReleased() {
			return dateReleased;
		}
		
		public void setDateReleased(LocalDate dateReleased) {
			this.dateReleased = dateReleased;
		}
		
		public LocalTime getMovieRunningTime() {
			return movieRunningTime;
		}
		
		public void setMovieRunningTime(LocalTime movieRunningTime) {
			this.movieRunningTime = movieRunningTime;
		}
		
		@Override
		public String toString() {
			return "MovieDTO [movieId=" + movieId + ", movieTitle=" + movieTitle + ", dateReleased=" + dateReleased
					+ ", movieRunningTime=" + movieRunningTime + "]";
		}
		
		public static Movie prepareMovieEntity(MovieDTO movieDTO)
		{
			Movie movieEntity = new Movie();
			movieEntity.setMovieId(movieDTO.getMovieId());   
			movieEntity.setMovieTitle(movieDTO.getMovieTitle());  
			movieEntity.setDateReleased(movieDTO.getDateReleased());   
			movieEntity.setMovieRunningTime(movieDTO.getMovieRunningTime());  
			return movieEntity;
		}
}
